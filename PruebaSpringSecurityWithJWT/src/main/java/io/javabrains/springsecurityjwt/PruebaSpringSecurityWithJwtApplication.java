package io.javabrains.springsecurityjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaSpringSecurityWithJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaSpringSecurityWithJwtApplication.class, args);
	}

}
