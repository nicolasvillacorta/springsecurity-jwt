package io.javabrains.springsecurityjwt.security;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		
		/* Este metodo es el que carga el usuario de la base de datos, o donde sea que este, en este ejemplo voy 
		 * a usar un usuario hardcodeado. Hace falta crear mi propia clase user si necesito que tenga 
		 * parametros customizados. */
		return new User("foo", "foo", new ArrayList<>());
		
	}
	
}
